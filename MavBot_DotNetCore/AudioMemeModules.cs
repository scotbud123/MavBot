﻿using Discord.Audio;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace MavBot
{
    public class AudioMemeModules : ModuleBase<SocketCommandContext> //must be public
    {
        [Command("name", RunMode = RunMode.Async), Summary("The one true answer to the question: \"What is your name?\"")]
        public async Task Name()
        {
            string path = $"{System.IO.Directory.GetCurrentDirectory()}/music_resources/name_jeff.mp3";

            var voiceChannel = (Context.User as SocketGuildUser)?.VoiceChannel;
            if (voiceChannel != null) //The person invoking the command is currently connected to a voice channel
            {
                joinPlayLeave(voiceChannel, path);
            }
            else //The person invoking the command is NOT currently connected to a voice channel, just print the text version instead
            {
                await ReplyAsync("My name is Jeff!");
            }
        }

        [Command("mydudes", RunMode = RunMode.Async), Summary("What day is it my dudes?")]
        public async Task MyDudes()
        {
            string path = $"{System.IO.Directory.GetCurrentDirectory()}/music_resources/my_dudes.mp3";

            DayOfWeek today = DateTime.Now.DayOfWeek;

            if (today == DayOfWeek.Wednesday)
            {
                var voiceChannel = (Context.User as SocketGuildUser)?.VoiceChannel;
                if (voiceChannel != null) //The person invoking the command is currently connected to a voice channel
                {
                    joinPlayLeave(voiceChannel, path);
                }
                else //The person invoking the command is NOT currently connected to a voice channel, just print the text version instead
                {
                    await ReplyAsync("It's Wednesday my dudes! AHHHHHHHHHHHHHHHHHHHHHHHHH!");
                }
            }
            else //today is not Wednesday
            {
                await ReplyAsync("It's not Wednesday my dudes! :cry: Try again on Wednesday...");
            }
        }

        [Command("hi", RunMode = RunMode.Async), Summary("Why, hello there!")]
        public async Task Hi()
        {
            string path = $"{System.IO.Directory.GetCurrentDirectory()}/music_resources/hello_there.mp3";

            var voiceChannel = (Context.User as SocketGuildUser)?.VoiceChannel;
            if (voiceChannel != null) //The person invoking the command is currently connected to a voice channel
            {
                joinPlayLeave(voiceChannel, path);
            }
            else //The person invoking the command is NOT currently connected to a voice channel, just print the text version instead
            {
                await ReplyAsync("Hello There!");
            }
        }

        [Command("drugs", RunMode = RunMode.Async), Summary("I want em!")]
        public async Task Drugs()
        {
            string path = $"{System.IO.Directory.GetCurrentDirectory()}/music_resources/drugs.mp3";

            var voiceChannel = (Context.User as SocketGuildUser)?.VoiceChannel;
            if (voiceChannel != null) //The person invoking the command is currently connected to a voice channel
            {
                joinPlayLeave(voiceChannel, path);
            }
            else //The person invoking the command is NOT currently connected to a voice channel, just print the text version instead
            {
                await ReplyAsync("Drugs...what's wrong with em? I want em!");
            }
        }

        [Command("mana", RunMode = RunMode.Async), Summary("Duri's Status")]
        public async Task Mana()
        {
            string path = $"{System.IO.Directory.GetCurrentDirectory()}/music_resources/mana.mp3";

            var voiceChannel = (Context.User as SocketGuildUser)?.VoiceChannel;
            if (voiceChannel != null) //The person invoking the command is currently connected to a voice channel
            {
                joinPlayLeave(voiceChannel, path);
            }
            else //The person invoking the command is NOT currently connected to a voice channel, just print the text version instead
            {
                await ReplyAsync("YOU HAVE NO MANA!!!");
            }
        }

        [Command("triggered", RunMode = RunMode.Async), Summary("Ironically, also Duri's status.")]
        public async Task Triggered()
        {
            string path = $"{System.IO.Directory.GetCurrentDirectory()}/music_resources/ree.mp3";

            var voiceChannel = (Context.User as SocketGuildUser)?.VoiceChannel;
            if (voiceChannel != null) //The person invoking the command is currently connected to a voice channel
            {
                joinPlayLeave(voiceChannel, path);
            }
            else //The person invoking the command is NOT currently connected to a voice channel, just print the text version instead
            {
                await ReplyAsync(":rage: REEEEEEE! :rage:");
            }
        }

        [Command("tendies", RunMode = RunMode.Async), Summary("Ironically, also Duri's status.")]
        public async Task Tendies()
        {
            string path = $"{System.IO.Directory.GetCurrentDirectory()}/music_resources/tendies.mp3";

            var voiceChannel = (Context.User as SocketGuildUser)?.VoiceChannel;
            if (voiceChannel != null) //The person invoking the command is currently connected to a voice channel
            {
                joinPlayLeave(voiceChannel, path);
            }
            else //The person invoking the command is NOT currently connected to a voice channel, just print the text version instead
            {
                await ReplyAsync(":rage: CHICKEN, GIVE ME TENDIES! :rage:");
            }
        }

        [Command("jedi", RunMode = RunMode.Async), Summary("Evan sucks!")]
        public async Task Jedi()
        {
            string path = $"{System.IO.Directory.GetCurrentDirectory()}/music_resources/jedi.mp3";

            var voiceChannel = (Context.User as SocketGuildUser)?.VoiceChannel;
            if (voiceChannel != null) //The person invoking the command is currently connected to a voice channel
            {
                joinPlayLeave(voiceChannel, path);
            }
            else //The person invoking the command is NOT currently connected to a voice channel, just print the text version instead
            {
                await ReplyAsync("The dark times...");
            }
        }

        [Command("citizens", RunMode = RunMode.Async), Summary("of Dalaran!")]
        public async Task Citizens()
        {
            string path = $"{System.IO.Directory.GetCurrentDirectory()}/music_resources/citizens.mp3";

            var voiceChannel = (Context.User as SocketGuildUser)?.VoiceChannel;
            if (voiceChannel != null) //The person invoking the command is currently connected to a voice channel
            {
                joinPlayLeave(voiceChannel, path);
            }
            else //The person invoking the command is NOT currently connected to a voice channel, just print the text version instead
            {
                await ReplyAsync("of Dalaran!");
            }
        }

        [Command("legends", RunMode = RunMode.Async), Summary("never die!")]
        public async Task Legends()
        {
            string path = $"{System.IO.Directory.GetCurrentDirectory()}/music_resources/legends.mp3";

            var voiceChannel = (Context.User as SocketGuildUser)?.VoiceChannel;
            if (voiceChannel != null) //The person invoking the command is currently connected to a voice channel
            {
                joinPlayLeave(voiceChannel, path);
            }
            else //The person invoking the command is NOT currently connected to a voice channel, just print the text version instead
            {
                await ReplyAsync("...never die!");
            }
        }

        private async void joinPlayLeave(SocketVoiceChannel voiceChannel, string path)
        {
            try
            {
                var output = CreateStream(path).StandardOutput.BaseStream;
                var myAudioClient = await voiceChannel.ConnectAsync();

                //You can change the bitrate of the outgoing stream with an additional argument to CreatePCMStream().
                //If not specified, the default bitrate is 96*1024.
                var stream = myAudioClient.CreatePCMStream(AudioApplication.Voice);
                await output.CopyToAsync(stream);
                await stream.FlushAsync().ConfigureAwait(false);

                await myAudioClient.StopAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception details: \n{e.ToString()}");
                await ReplyAsync("Please maintain a limit of 1 audio command at a time.");
            }
        }

        private Process CreateStream(string path)
        {
            return Process.Start(new ProcessStartInfo
            {
                //Windows Version
                FileName = $"{System.IO.Directory.GetCurrentDirectory()}/music_resources/ffmpeg.exe",
                //Linux Version
                //FileName = "ffmpeg",
                Arguments = $"-hide_banner -loglevel panic -i {path} -ac 2 -f s16le -af \"volume = 0.5\" -ar 48000 pipe:1",
                UseShellExecute = false,
                RedirectStandardOutput = true
            });
        }
    }
}
