﻿using System;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using Discord.Commands;
using System.Reflection;

namespace MavBot
{
    public class Program
    {
        public readonly DiscordSocketClient _myClient = new DiscordSocketClient(); //Decalre the main socket for the bot's connection

        // Keep the CommandService and IServiceCollection around for use with commands.
        private readonly IServiceCollection _map = new ServiceCollection();
        private readonly CommandService _commands = new CommandService();

        static void Main(string[] args)
                => new Program().MainAsync().GetAwaiter().GetResult();

        public async Task MainAsync()
        {
            _myClient.Log += Log; //Hook the client's log even to our log handler
            _commands.Log += Log; //Same for the command service's log

            await this.InitCommands();

            await _myClient.LoginAsync(TokenType.Bot, ConfigurationManager.AppSettings.Get("botToken")); //THIS IS TO KEEP OUR TOKEN PRIVATE! VERY IMPORTANT!
            await _myClient.StartAsync();

            // Block this task until the program is closed.
            await Task.Delay(-1);
        }

        // HELPER FUNCTIONS

        private IServiceProvider _services;

        private async Task InitCommands()
        {
            // Repeat this for all the service classes
            // and other dependencies that your commands might need.
            //_map.AddSingleton(new SomeServiceClass());

            // When all your required services are in the collection, build the container.
            // Tip: There's an overload taking in a 'validateScopes' bool to make sure
            // you haven't made any mistakes in your dependency graph.
            _services = _map.BuildServiceProvider();

            // Either search the program and add all Module classes that can be found.
            // Module classes *must* be marked 'public' or they will be ignored.
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly());
            // Or add Modules manually if you prefer to be a little more explicit:
            //await _commands.AddModuleAsync<SomeModule>();

            // Subscribe a handler to see if a message invokes a command.
            _myClient.MessageReceived += HandleCommandAsync;
        }

        private async Task HandleCommandAsync(SocketMessage arg) {
            // Bail out if it's a System Message.
            var message = arg as SocketUserMessage;
            if (message == null) return;

            // Create a number to track where the prefix ends and the command begins
            int position = 0;
            // Replace the '!' with whatever character
            // you want to prefix your commands with.
            // Uncomment the second half if you also want
            // commands to be invoked by mentioning the bot instead.
            if (message.HasCharPrefix('/', ref position) || message.HasMentionPrefix(_myClient.CurrentUser, ref position))
            {
                // Create a Command Context
                var context = new SocketCommandContext(_myClient, message);

                // Execute the command. (result does not indicate a return value, 
                // rather an object stating if the command executed succesfully).
                var result = await _commands.ExecuteAsync(context, position, _services);

                // Uncomment the following lines to make the bot
                // send a message if it failed (not advised for most situations).
                if (!result.IsSuccess && result.Error != CommandError.UnknownCommand)
                    await message.Channel.SendMessageAsync(result.ErrorReason);
            }
        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }
    }
}
