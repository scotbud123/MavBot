﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using System.Threading;

namespace MavBot
{
    public class MyModules : ModuleBase<SocketCommandContext> //must be public
    {
        const ulong OWNER_USER_ID = 130292382200627200; //Change this to your own UserID

        [Command("ping"), Summary("Sends back a pong! Also tells you the current latency of the bot.")]
        //public async Task Ping([Remainder, Summary("The ping to print")] int ping) {
        //FIRST LINE IS WRONG, THAT'S HOW YOU USE A PARAMETER PASSED BY THE USER IN DISCORD WHEN THEY TYPE THE COMMAND
        public async Task Ping()
        {
            // ReplyAsync is a method on ModuleBase
            await ReplyAsync($"Pong! Also, the current ping is: {Context.Client.Latency}");
        }

        [Command("help"), Summary("Displays the help message with the list of all commands.")]
        public async Task Help()
        {
            //Perhaps put this in a general data file, maybe the "Services" files I was speaking with people in the Discord about
            await ReplyAsync("```\n" +
                             "General Commands:\n-----------------------------------------------------------------------------\n" +
                             "help\t\t| Displays this help message\n" +
                             "ping\t\t| Sends back a pong! Also tells you the current latency of the bot\n" +
                             "armory\t  | WoWProgress, WarcraftLogs, and Armory links for our WoW characters\n" +
                             "\nMemes:\n-----------------------------------------------------------------------------\n" +
                             "diet\t\t| A Reminder to all of the one true diet\n" +
                             "name\t\t| The one true answer to the question: \"What is your name?\"\n" +
                             "mydudes\t | What day is it my dudes? (Hint: Only works on Wednesday)\n" +
                             "hi\t\t  | Why, hello there!\n" +
                             "drugs\t   | Ech!\n" + 
                             "mana\t\t| Duri's Status\n" + 
                             "triggered   | Ironically, also Duri's status\n" +
                             "tendies\t | Give me!\n" + 
                             "citizens\t| Of...\n```"); //This shouldn't be hardcoded I know, it's also out of date too, needs updates big time
        }

        [Command("diet"), Summary("A Reminder to all of the one true diet.")]
        public async Task Diet()
        {
            await ReplyAsync("ケツを食べる");
        }

        [Command("armory"), Summary("A Reminder to all of the one true diet.")]
        public async Task Armory()
        {
            //Maybe pass in a string that can choose to only show one character?

            var myEmbed = new EmbedBuilder();

            myEmbed.AddInlineField("__**WARNING!**__", "These values are very out of date, this needs updating.");

            myEmbed.AddInlineField("__**Dio's Links:**__", "WoWProgress: https://www.wowprogress.com/character/us/azgalor/vega \n" +
                                                   "WarcraftLogs: https://www.warcraftlogs.com/character/us/azgalor/vega \n" +
                                                   "WoW Armory: https://us.battle.net/wow/en/character/azgalor/vega");
            myEmbed.AddInlineField("__**Cybra's Links:**__", "WoWProgress: https://www.wowprogress.com/character/us/kirin-tor/cybrenä \n" +
                                                   "WarcraftLogs: https://www.warcraftlogs.com/character/us/kirin-tor/cybrenä \n" +
                                                   "WoW Armory: https://us.battle.net/wow/en/character/kirin-tor/cybrenä");
            myEmbed.AddInlineField("__**Duri's Links:**__", "WoWProgress: https://www.wowprogress.com/character/us/kirin-tor/diffuse \n" +
                                                   "WarcraftLogs: https://www.warcraftlogs.com/character/us/kirin-tor/diffuse \n" +
                                                   "WoW Armory: https://us.battle.net/wow/en/character/kirin-tor/diffuse");
            myEmbed.AddInlineField("__**Gryllus' Links:**__", "WoWProgress: https://www.wowprogress.com/character/us/scarlet-crusade/gryllus \n" +
                                                   "WarcraftLogs: https://www.warcraftlogs.com/character/us/scarlet-crusade/gryllus \n" +
                                                   "WoW Armory: https://us.battle.net/wow/en/character/scarlet-crusade/gryllus");

            await Context.Channel.SendMessageAsync("", false, myEmbed);
        }

        //FOR TESTING
        [STAThread] //Is this needed? Double check once everything is working
        [Command("test", RunMode = RunMode.Async)]
        public async Task Test()
        {
            var invokingUserID = Context.User.Id;

            if (invokingUserID == OWNER_USER_ID)
            {
                //var voiceChannel = (Context.User as SocketGuildUser)?.VoiceChannel;

                //await Context.Guild.AudioClient.StopAsync;

                await ReplyAsync(AppDomain.CurrentDomain.BaseDirectory);
                await ReplyAsync(System.IO.Directory.GetCurrentDirectory());
                await ReplyAsync($"{System.IO.Directory.GetCurrentDirectory()}\\music_resources\\ffmpeg.exe");

                //TestForm myForm = new TestForm();
                //myForm.Show();

                //CODE UNDER THIS IS GOOD, JUST NOT FOR .NET CORE
                //Thread.CurrentThread.SetApartmentState(ApartmentState.STA);
                //Application.Run(new TestForm());
            }
            else
            {
                await ReplyAsync("Why the fuck did you think you could use this? It's not even in the help command...");
            }
        }

        [Command("shutdown", RunMode = RunMode.Async), Summary("Properly shuts down the bot.")]
        public async Task Shutdown()
        {
            var invokingUserID = Context.User.Id;

            if (invokingUserID == OWNER_USER_ID)
            {
                await ReplyAsync("Bye! :wave:");
                //Set the Status of the bot to Invisible because there's a bug with corefx's websocket not sending a proper exit code to Discord
                await Context.Client.SetStatusAsync(UserStatus.Invisible);
                await Context.Client.StopAsync();
                Environment.Exit(0);
            }
            else
            {
                await ReplyAsync("You do not have permission to use that command!");
            }
        }
    }
}
