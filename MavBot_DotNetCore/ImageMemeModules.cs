﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MavBot_DotNetCore
{
    public class ImageMemeModules : ModuleBase<SocketCommandContext> //must be public
    {
        [Command("duritriggered"), Summary("")]
        public async Task DuriTriggered()
        {
            await ReplyAsync("https://i.imgur.com/pdVdDAb.png");
        }

        [Command("nicedude"), Summary("")]
        public async Task NiceDude()
        {
            await ReplyAsync("https://i.imgur.com/AivdkaX.jpg");
        }

        [Command("duri"), Summary("")]
        public async Task Duri()
        {
            await ReplyAsync("https://i.imgur.com/nV0NLFj.png");
        }

        [Command("doit"), Summary("")]
        public async Task DoIt()
        {
            await ReplyAsync("https://i.imgur.com/wCnsQPH.png");
        }
    }
}
