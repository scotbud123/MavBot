#!/bin/bash

pushd MavBot_DotNetCore
pushd MavBot_DotNetCore

# I don't care about sudo not being "good practice" in scripts
sudo dotnet run

popd
popd